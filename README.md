# backend_repo

Ce projet est destiné aux test de diverse configuration et n'est donc pas complètement sécuriséau niveau des dossiers/fichers présent sur le repository.

## Installing Chocolatey and Make with PowerShell On Windows

To install Chocolatey and Make using PowerShell, follow the steps below:

Open PowerShell: Press Win + R, type powershell, and press Enter.

Install Chocolatey:
Execute the following command in the PowerShell window:

powershell
Copy code
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
Install Chocolatey via PowerShell:
After Chocolatey is installed, run the command:

powershell
Copy code
install-chocolatey-powershell
Restart PowerShell:
Close and reopen the PowerShell window to ensure changes take effect.

Install Make:
Once Chocolatey is installed and PowerShell is restarted, install Make by executing:

powershell
Copy code
choco install make
After following these steps, Make should be installed on your system. You can verify the installation by running make --version in the PowerShell window.

## Configure Sonarqube

Run this command on terminal : docker run -d --name sonarqube -e SONAR_ES_BOOTSTRAP_CHECKS_DISABLE=true -p 9000:9000 sonarqube:latest

Go on http://localhost:9000

Log in with admin / admin then change your password

Start creating a new projet, set a name, project_key.

Select Gilab CI for analysis method.

#  propreté du code

Lancer cette commande dans le terminal : make prettier

# gestion de l'authentification

L'authentification a été faites a la main avec la gestion d'un jsonwebtoken.

Changer de répertoire et aller dans api/src/controller/authController.js pour acceder aux fonctions de d'inscription et de connexion.

A la connexion , un jwt est créer contenant l'email et le role de l'utilisateur dans son payload.

# gestion des permissions

Changer de répertoire et aller dans api/src/middleware/authMiddleware.js pour acceder aux middleware.

le middleware vérifie que le jwt est bien valide, récupère les permissions de l'utilisateur en fonction du role et passe à la suite si c'est le cas.

# ZAP , test de sécurité

La configuration CI de Zap a été effectué mais n'est pas fonctionnel.

Le répertoire est celui-ci : api/wrk/zap-secu.yml
